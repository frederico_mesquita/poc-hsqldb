package hsqldb;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hsqldb.Server;

public class HSqlDB {
	private static final Logger l = Logger.getLogger(HSqlDB.class.getName());
	
	// JDBC Data and Database URL                                   
	static final String JDBC_DRIVER = "org.hsqldb.jdbcDriver";  
	static final String DB_PORT = "9001";
	static final String DB_HOST = "127.0.0.1";
	static final String DB_NAME = "mdb";
	static final String DB_URL = "jdbc:hsqldb:hsql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME;    
	
	//  Database credentials                                               
	static final String USER = "sa";                                   
	static final String PASS = "";  

	public static void main(String[] args) {
		Connection conn = null;                                                
		Statement stmt = null;
		
		Server hsqlServer = null;
		                                                                       
		try{
			hsqlServer = new Server();

            hsqlServer.setLogWriter(null);
            hsqlServer.setSilent(true);

            hsqlServer.setDatabaseName(0, DB_NAME);
            hsqlServer.setDatabasePath(0, "mem:" + DB_NAME);
            hsqlServer.setPort(Integer.parseInt(DB_PORT));
            hsqlServer.start();
		                                                          
		   // Register JDBC Driver                                
		   Class.forName(JDBC_DRIVER);                            
		                                                          
		   // Open a connection                                   
		   conn = DriverManager.getConnection(DB_URL, USER, PASS);
		   
		   conn.prepareStatement(
                   "create table tabela ( id INTEGER, "+
                   "label VARCHAR(50));")
                   .execute();
		   
		   for(int iCount = 0; iCount < 100; iCount++)
			   conn.prepareStatement(
	                   "insert into tabela(id, label) "+
	                   "values (" + iCount + ", 'test_" + iCount + "');")
	                   .execute();
		                                                          
		   // Execute a query                                     
		   stmt = conn.createStatement();                         
		   String sql1 = "SELECT * from tabela;";                                
		   ResultSet rs1 = stmt.executeQuery(sql1);               
		                                                          
		   // Extract data from result set                        
		   while(rs1.next()){                                      
		      //DISPLAY VALUES                                    
		      System.out.println("Identifier: " + rs1.getInt("id") + " - Label: " + rs1.getString("label"));                             
		   }                          
                                                                
		   rs1.close();
		   hsqlServer.stop();
		}catch(SQLException se){
			l.log(Level.SEVERE, se.getMessage());
		}catch(Exception e){        
			l.log(Level.SEVERE, e.getMessage());                   
		}finally{
			try{                                     
				if(hsqlServer.getState() != 16)
					hsqlServer.shutdown();
				hsqlServer = null;
			}catch(Exception e){ 
				l.log(Level.SEVERE, e.getMessage());
			}
			
			try{                                     
				if(stmt != null)                        
					conn.close();                      
			}catch(SQLException se){ 
				l.log(Level.SEVERE, se.getMessage());
			}
		   
			try{                                     
				if(conn != null)                        
					conn.close();                      
			}catch(SQLException se){
				l.log(Level.SEVERE, se.getMessage());
			}                                               
		}  
	}
}
