PoC HSQLDB

With this code I want to show a simple way to use an in-memory database inside a Java application.

Getting Started

Import this maven projet to your favorite IDE, compile and run.

Prerequisites

Java 1.8 or above.

Running the tests

There are no tests inside this PoC. Just import, compile and run. It�s a very simple one.

After run this code the following just occurred:

  . The database was created in memory;

  . A table was created;
  
  . Data was inserted;
  
  . The content was read and printed;
  
  . The memory was released.

Add additional notes about how to deploy this on a live system

Built With

Maven - Dependency Management

Authors

Frederico Mesquita

https://codeandbuild.blogspot.com.br/

License

This project is licensed under the MIT License - see the LICENSE.md file for details

Acknowledgments

http://hsqldb.org/

https://www.ibm.com/developerworks/systems/library/es-zos/index.html